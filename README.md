# CheckStar API

It is a simple managing products service which allows the client not only to create a product, but also to list all the registered products, list one single product by the id, update the product filtered by the barcode registered and delete the product.

## How to get started (global):

This api has been deployded to heroku, so here is the link to acces the api.

```
https://gitlab.com/DeniseBelo/api-checkstar
```

Now you have to use a api client program, as insomnia, for example, so you can access and test the api.

## How to get started (locally):

To run this service in your local you need to do some steps, starting with cloning this project from gitlab. Click on this link:

```
https://gitlab.com/DeniseBelo/api-checkstar
```

Clone with the ssh option. After that, create a new diretory in your local, inside this directory use:

```
git clone <paste the url copied>
```

Move in the directory created and run you code editor (you can use visual studio code, for instance).
Now open you terminal inside de project and if you have yarn previously installed in you machine, run:

```
yarn install
```

You will install all the dependencies you need this way. And now you can see the code.

In case you don't have yarn install follow these steps:

Yarn installation. In your terminal type the following code:

```
npm install --global yarn
```

With yarn installed go to the directory of the project:

```
cd <your_project>
```

Run the code on your local. Open a terminal inside your project directory, and install the project dependencies:

```
yarn install
```

Run the service locally:

```
yarn dev
```

Now you have to use insomnia or other platform to access the API. Configure this platform with you localhost address in the port 3000.

- the service will be running at http://127.0.0.1:3000/

## Routes

## POST /products

Create a new product.

- Does not need authentication.
- barcode must have 13 digits.

### Request example:

```
{
	"name": "Burguer",
    "fullPrice": 17.99,
    "discountPrice": 14.99,
    "description": "Mix of meat, jalapenos, cheese and tomatoes",
    "img": "https://media-cdn.tripadvisor.com/media/photo-s/1c/68/34/58/carro-chefe-da-casa-criado.jpg",
    "barcode": "1427853125477"
}
```

### Response example:

- STATUS: 201 CREATED

```
{
  "id": 4,
  "name": "Burguer",
  "fullPrice": 17.99,
  "discountPrice": 14.99,
  "description": "Mix of meat, jalapenos, cheese and tomatoes",
  "img": "https://media-cdn.tripadvisor.com/media/photo-s/1c/68/34/58/carro-chefe-da-casa-criado.jpg",
  "barcode": "1427853125477",
  "updatedAt": "2022-01-21T23:38:39.695Z",
  "createdAt": "2022-01-21T23:38:39.695Z"
}
```

## GET /products

List all the registered products.

- Does not need authentication.

### Request example:

```
no body
```

### Response example:

- STATUS: 200 OK

```
[
  {
    "id": 3,
    "name": "Burguer",
    "fullPrice": 17.99,
    "discountPrice": 14.99,
    "description": "Mix of meat, jalapenos, cheese and tomatoes",
    "img": "https://media-cdn.tripadvisor.com/media/photo-s/1c/68/34/58/carro-chefe-da-casa-criado.jpg",
    "barcode": "1427853125477",
    "createdAt": "2022-01-21T21:17:18.754Z",
    "updatedAt": "2022-01-21T21:17:18.754Z"
  },
  {
    "id": 2,
    "name": "Amazing Pizza",
    "fullPrice": 22.99,
    "discountPrice": 18.99,
    "description": "Peperroni, cheese, tomatoes sauce and onions",
    "img": "https://img.itdg.com.br/tdg/images/blog/uploads/2019/05/pizza.jpg",
    "barcode": "1427853125436",
    "createdAt": "2022-01-21T19:14:09.875Z",
    "updatedAt": "2022-01-21T21:18:18.651Z"
  }
]
```

## GET /products/"id"

This route get just one product, filtered by it's id.

- Does not need authentication.
- Use the product id in the route's parameter url.

### Request example:

```
    no body
```

### Response example:

- STATUS: 200 OK

```
{
  "id": 2,
  "name": "Amazing Pizza",
  "fullPrice": 22.99,
  "discountPrice": 18.99,
  "description": "Peperroni, cheese, tomatoes sauce and onions",
  "img": "https://img.itdg.com.br/tdg/images/blog/uploads/2019/05/pizza.jpg",
  "barcode": "1427853125436",
  "createdAt": "2022-01-21T19:14:09.875Z",
  "updatedAt": "2022-01-21T21:18:18.651Z"
}
```

## PATCH /products/"barcode"

This update allows the client to update the product data.

- Does not need authentication.
- Use the product barcode in the route's parameter url.

### Request example:

```
{
	"name": "newName"
}
```

### Response example:

- STATUS: 200 OK

```
{
  "id": 2,
  "name": "newName",
  "fullPrice": 22.99,
  "discountPrice": 18.99,
  "description": "Peperroni, cheese, tomatoes sauce and onions",
  "img": "https://img.itdg.com.br/tdg/images/blog/uploads/2019/05/pizza.jpg",
  "barcode": "1427853125436",
  "createdAt": "2022-01-21T19:14:09.875Z",
  "updatedAt": "2022-01-21T23:46:28.096Z"
}
```

## DELETE /products/"id"

This route will delete one product, filtered by it's id.

- Does not need authentication.
- Use the product id in the route's parameter url.

### Request example:

```
    no body
```

### Response example:

- STATUS: 204 NO CONTENT

```
No body returned for response
```

## Technologies

- Node.js;
- Express;
- Sequelize;
- Javascript;
- Yup;
- Postgres;
- Heroku;

## Licenses

MIT
