import app from "./app";
import database from "./config/database/db";
(async () => {
  try {
    const product = await database.sync();
  } catch (error) {
    console.log(error);
  }
})();

const port = process.env.PORT;

app.listen(port, () => {
  console.log(`App rodando na porta: http://localhost:${port}`);
});
