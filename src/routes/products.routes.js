import { Router } from "express";
import { productSchema } from "../models/product.validation.model";
import { validate } from "../middlewares/validate.schema";
import {
  create,
  list,
  retrieve,
  update,
  destroy,
} from "../controllers/product.controller.js";

const route = Router();

export const productsRoutes = (app) => {
  route.post("", validate(productSchema), create);
  route.get("", list);
  route.get("/:id", retrieve);
  route.patch("/:barcode", update);
  route.delete("/:id", destroy);

  app.use("/products", route);
};
