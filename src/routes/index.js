import { productsRoutes } from "./products.routes";
import express from "express";

export const routes = (app) => {
  app.use(express.json());
  productsRoutes(app);
};
