import * as yup from "yup";

export const productSchema = yup.object().shape({
  name: yup.string().required(),
  fullPrice: yup.number().required().positive(),
  discountPrice: yup.number().required().positive(),
  description: yup.string().required(),
  img: yup.string(),
  barcode: yup.string().length(13).required(),
});
