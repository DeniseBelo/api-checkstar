import Sequelize from "sequelize";
import database from "../config/database/db";

export const Product = database.define("product", {
  id: {
    type: Sequelize.INTEGER,
    autoIncrement: true,
    allowNull: false,
    primaryKey: true,
  },
  name: {
    type: Sequelize.STRING,
    allowNull: false,
  },
  fullPrice: {
    type: Sequelize.DOUBLE,
  },
  discountPrice: {
    type: Sequelize.DOUBLE,
  },
  description: {
    type: Sequelize.STRING,
  },
  img: {
    type: Sequelize.STRING,
  },
  barcode: {
    type: Sequelize.STRING,
  },
});
