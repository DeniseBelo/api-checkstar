"use strict";Object.defineProperty(exports, "__esModule", {value: true}); function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }var _sequelize = require('sequelize'); var _sequelize2 = _interopRequireDefault(_sequelize);
var _db = require('../config/database/db'); var _db2 = _interopRequireDefault(_db);

 const Product = _db2.default.define("product", {
  id: {
    type: _sequelize2.default.INTEGER,
    autoIncrement: true,
    allowNull: false,
    primaryKey: true,
  },
  name: {
    type: _sequelize2.default.STRING,
    allowNull: false,
  },
  fullPrice: {
    type: _sequelize2.default.DOUBLE,
  },
  discountPrice: {
    type: _sequelize2.default.DOUBLE,
  },
  description: {
    type: _sequelize2.default.STRING,
  },
  img: {
    type: _sequelize2.default.STRING,
  },
  barcode: {
    type: _sequelize2.default.STRING,
  },
}); exports.Product = Product;
