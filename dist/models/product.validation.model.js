"use strict";Object.defineProperty(exports, "__esModule", {value: true}); function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { newObj[key] = obj[key]; } } } newObj.default = obj; return newObj; } }var _yup = require('yup'); var yup = _interopRequireWildcard(_yup);

 const productSchema = yup.object().shape({
  name: yup.string().required(),
  fullPrice: yup.number().required().positive(),
  discountPrice: yup.number().required().positive(),
  description: yup.string().required(),
  img: yup.string(),
  barcode: yup.string().length(13).required(),
}); exports.productSchema = productSchema;
