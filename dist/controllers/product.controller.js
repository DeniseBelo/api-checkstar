"use strict";Object.defineProperty(exports, "__esModule", {value: true});var _productmodel = require('../models/product.model');

 const create = async (req, res) => {
  const data = req.validatedData;
  const productCreate = await _productmodel.Product.create({
    ...data,
  });
  res.status(201).json(productCreate);
}; exports.create = create;

 const list = async (req, res) => {
  const products = await _productmodel.Product.findAll();
  if (products === undefined) {
    return res.status(404).json({ message: "No product registered." });
  }
  res.status(200).json(products);
}; exports.list = list;

 const retrieve = async (req, res) => {
  const id = parseInt(req.params.id);
  if (isNaN(id)) {
    return res.status(400).json({ message: "Id must be an integer." });
  }
  const product = await _productmodel.Product.findByPk(id);
  if (product === null) {
    return res.status(404).json({ message: "Id not registered." });
  }
  res.status(200).json(product);
}; exports.retrieve = retrieve;

 const update = async (req, res) => {
  const barcode = req.params.barcode;
  try {
    const product = await _productmodel.Product.update(req.body, {
      where: { barcode },
      returning: true,
      plain: true,
    });
    res.status(200).json(product[1]);
  } catch (e) {
    return res.status(404).json({ message: "Invalid barcode." });
  }
}; exports.update = update;

 const destroy = async (req, res) => {
  const id = req.params.id;
  if (isNaN(id)) {
    return res.status(400).json({ message: "Id must be an integer." });
  }
  const productToBeDeleted = await _productmodel.Product.findByPk(id);
  if (productToBeDeleted === null) {
    return res.status(404).json({ message: "Product not registered." });
  }
  const deletedProduct = _productmodel.Product.destroy({ where: { id: id } });
  res.status(204).json("");
}; exports.destroy = destroy;
