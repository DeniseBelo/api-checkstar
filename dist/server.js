"use strict"; function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }var _app = require('./app'); var _app2 = _interopRequireDefault(_app);
var _db = require('./config/database/db'); var _db2 = _interopRequireDefault(_db);
(async () => {
  try {
    const product = await _db2.default.sync();
  } catch (error) {
    console.log(error);
  }
})();

const port = 3000;

_app2.default.listen(port, () => {
  console.log(`App rodando na porta: http://localhost:${port}`);
});
