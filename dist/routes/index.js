"use strict";Object.defineProperty(exports, "__esModule", {value: true}); function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }var _productsroutes = require('./products.routes');
var _express = require('express'); var _express2 = _interopRequireDefault(_express);

 const routes = (app) => {
  app.use(_express2.default.json());
  _productsroutes.productsRoutes.call(void 0, app);
}; exports.routes = routes;
