"use strict";Object.defineProperty(exports, "__esModule", {value: true});var _express = require('express');
var _productvalidationmodel = require('../models/product.validation.model');
var _validateschema = require('../middlewares/validate.schema');






var _productcontrollerjs = require('../controllers/product.controller.js');

const route = _express.Router.call(void 0, );

 const productsRoutes = (app) => {
  route.post("", _validateschema.validate.call(void 0, _productvalidationmodel.productSchema), _productcontrollerjs.create);
  route.get("", _productcontrollerjs.list);
  route.get("/:id", _productcontrollerjs.retrieve);
  route.patch("/:barcode", _productcontrollerjs.update);
  route.delete("/:id", _productcontrollerjs.destroy);

  app.use("/products", route);
}; exports.productsRoutes = productsRoutes;
