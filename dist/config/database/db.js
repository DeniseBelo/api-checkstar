"use strict"; function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }var _sequelize = require('sequelize'); var _sequelize2 = _interopRequireDefault(_sequelize);

const sequelize = new (0, _sequelize2.default)(
  "postgres://denise:1234@localhost:5432/checkstar",
  { dialect: "postgres" }
);

module.exports = sequelize;
