"use strict";Object.defineProperty(exports, "__esModule", {value: true}); const validate = (schema) => async (req, res, next) => {
  const body = req.body;

  try {
    const validatedData = await schema.validate(body, {
      abortEarly: false,
      stripUnknown: true,
    });
    req.validatedData = validatedData;
    next();
  } catch (e) {
    return res.status(422).json({ [e.name]: e.errors });
  }
}; exports.validate = validate;
